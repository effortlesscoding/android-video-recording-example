package com.test.anhdao.appinstructions;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.test.anhdao.appinstructions.helpers.CamVideoRecorder;
import com.test.anhdao.appinstructions.helpers.ERecordState;
import com.test.anhdao.appinstructions.helpers.FilesManager;
import com.test.anhdao.appinstructions.helpers.IRecordingListener;
import com.test.anhdao.appinstructions.helpers.RecordingOptions;
import com.test.anhdao.appinstructions.ui.PieProgress;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IRecordingListener {

    @Override
    public void recordingStarted() {
        mMillisecondsElapsed = 0;
        mTimerHandler = new Handler();
        mTimerHandler.postDelayed(mTimerRunnable, 0);
    }

    @Override
    public void recordingResumed() {

    }

    private class StartRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switchToRecordState(ERecordState.Recording);
        }
    }

    private class PauseRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switchToRecordState(ERecordState.Paused);
        }
    }

    private class RetryRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switchToRecordState(ERecordState.OnStandByReady);
        }
    }

    private class AcceptRecordingListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            Log.i(TAG, "Accept tapped!");
            releaseAll();
            try {
                Intent goFilters = new Intent(MainActivity.this,
                        PreviewActivity.class);
                startActivity(goFilters);
                finish();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }



    private class RecordTimerRunnable implements Runnable {
        @Override
        public void run() {
            Log.i(TAG, "Timer runnable is still running!");
            ERecordState state = mRecorder.getRecordState();
            if (state != ERecordState.Paused && state != ERecordState.OnStandByReady) {
                mMillisecondsElapsed += MILLISECONDS_STEP;
            }
            float progress = (float)mMillisecondsElapsed / (float)RECORDING_MAX_MILLISECONDS;
            mCountdown.setProgress(progress);
            if (mMillisecondsElapsed < RECORDING_MAX_MILLISECONDS) {
                mTimerHandler.postDelayed(this, MILLISECONDS_STEP);
            } else if (state != ERecordState.Recorded){
                switchToRecordState(ERecordState.Recorded);
            }
        }
    }

    private CamVideoRecorder mRecorder;
    private SurfaceView mSurfaceView;
    private String TAG = this.getClass().getSimpleName();

    // Start / Pause / Approve
    private Button mActionButton;

    // Restart / Cancel
    //private Button mRetryButton;
    private Handler mTimerHandler;
    private int mMillisecondsElapsed;
    private final int MILLISECONDS_STEP = 100;
    private final int RECORDING_MAX_MILLISECONDS = 10000;
    private ViewGroup mTipsPanel;
    private TextView mTips;
    private PieProgress mCountdown;
    private Runnable mTimerRunnable;
    //private ProfileVideoVM mProfileVideo;
    private StartRecordingListener mStartRecordingHandler;
    private RetryRecordingListener mRetryRecordingHandler;
    private AcceptRecordingListener mAcceptRecordingHandler;
    private PauseRecordingListener mPauseRecordingHandler;


    private boolean askForPermissions(final int requestCode) {
        String audioPermission = Manifest.permission.RECORD_AUDIO;
        String cameraPermission = Manifest.permission.CAMERA;
        String storageReadPermission = Manifest.permission.READ_EXTERNAL_STORAGE;
        String storageWritePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        ArrayList<String> permissionsToAsk = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, audioPermission)
                != PackageManager.PERMISSION_GRANTED) {
            permissionsToAsk.add(audioPermission);
        }
        if (ContextCompat.checkSelfPermission(this, cameraPermission)
                != PackageManager.PERMISSION_GRANTED) {
            permissionsToAsk.add(cameraPermission);
        }

        if (ContextCompat.checkSelfPermission(this, storageReadPermission)
                != PackageManager.PERMISSION_GRANTED) {
            permissionsToAsk.add(storageReadPermission);
        }

        if (ContextCompat.checkSelfPermission(this, storageWritePermission)
                != PackageManager.PERMISSION_GRANTED) {
            permissionsToAsk.add(storageWritePermission);
        }

        if (permissionsToAsk.size() > 0) {
            String[] permissionsToAskArray = new String[permissionsToAsk.size()];
            for (int i = 0; i < permissionsToAsk.size(); i++) {
                permissionsToAskArray[i] = permissionsToAsk.get(i);
            }
            ActivityCompat.requestPermissions(this, permissionsToAskArray, requestCode);
            return true;
        }
        return false;
    }

    private void startRecording() {

                /* TODO: need to guarantee that the local video path exists! */
        File video = FilesManager.getVideoFile(MainActivity.this);
        File thumbnail = FilesManager.getThumbnailFile(MainActivity.this);
        RecordingOptions options = new RecordingOptions(
                video, thumbnail, RECORDING_MAX_MILLISECONDS / 1000);
        mRecorder = new CamVideoRecorder(
                MainActivity.this,
                mSurfaceView,
                options);
        mRecorder.startPreview();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mTimerRunnable              = new RecordTimerRunnable();
        mRetryRecordingHandler      = new RetryRecordingListener();
        mStartRecordingHandler      = new StartRecordingListener();
        mAcceptRecordingHandler     = new AcceptRecordingListener();
        mPauseRecordingHandler      = new PauseRecordingListener();

        mActionButton = (Button) findViewById(R.id.pvActionButton);
        mSurfaceView = (SurfaceView) findViewById(R.id.pvVideoPreview);
        mCountdown = (PieProgress) findViewById(R.id.pvCountdown);
        mTipsPanel = (ViewGroup) findViewById(R.id.pvTipsPanel);
        mTips = (TextView) findViewById(R.id.pvTips);
        mActionButton.setOnClickListener(mStartRecordingHandler);
        mSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                if (!askForPermissions(1)) {
                    startRecording();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mRecorder.stopRecording();
            }
        });
        // Not sure if this code even looks good.
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (1 == requestCode) {
            if (!askForPermissions(1)) {
                startRecording();
            }
        }
    }

    public void switchToRecordState(ERecordState newState) {
        Log.i(TAG, newState.toString());
        mRecorder.switchToRecordState(newState);
        switch (newState) {
            case OnStandByReady:
                mActionButton.setVisibility(View.VISIBLE);
                mActionButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_circle));
                mActionButton.setOnClickListener(mStartRecordingHandler);
                mMillisecondsElapsed = 0;
                mCountdown.setProgress(mMillisecondsElapsed);
                if (mTimerHandler != null) {
                    mTimerHandler.removeCallbacksAndMessages(null);
                }
                mTimerHandler = null;
                break;
            case Recording:
                mCountdown.setVisibility(View.VISIBLE);
                mActionButton.setOnClickListener(mPauseRecordingHandler);
                mActionButton.setVisibility(View.GONE);
                break;
            case Paused:
                break;
            case Recorded:
                mActionButton.setVisibility(View.VISIBLE);
                mActionButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_accept));
                mTips.setText("Done! Congrats!");
                mActionButton.setOnClickListener(mAcceptRecordingHandler);
                break;
        }
    }

    /***
     * Releases all handlers like timers and MediaRecorder
     * because these need manual memory release.
     */
    private void releaseAll() {
        if (mTimerHandler != null) {
            Log.d(TAG, "Handler destruction.");
            mTimerHandler.removeCallbacksAndMessages(null);
            mTimerHandler = null;
        }
        Log.d(TAG, "Camera destruction.");
        mRecorder.stopRecording();
    }

}
