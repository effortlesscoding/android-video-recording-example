package com.test.anhdao.appinstructions.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.test.anhdao.appinstructions.R;

import java.math.BigDecimal;

/**
 * Created by anhdao on 9/25/16.
 */
public class PieProgress extends View {

    private float mProgress;
    private int mSideSize;
    private final float ADJUST_FOR_12_OCLOCK = 270f;

    private Paint mBgPaint;
    private Paint mProgressPaint;
    private Paint mErrorPaint;

    public PieProgress(Context context, final AttributeSet attrs) {
        super(context, attrs);


        mErrorPaint = new Paint();
        this.mErrorPaint.setColor(ContextCompat.getColor(this.getContext(), R.color.colorAccent));
        this.mErrorPaint.setStyle(Paint.Style.FILL);
        this.mErrorPaint.setAntiAlias(true);
        this.mErrorPaint.setStrokeWidth(2);

        this.mBgPaint = new Paint();
        this.mBgPaint.setColor(ContextCompat.getColor(this.getContext(), R.color.black));
        this.mBgPaint.setStyle(Paint.Style.FILL);
        this.mBgPaint.setAntiAlias(true);
        this.mBgPaint.setStrokeWidth(2);

        this.mProgressPaint = new Paint();
        this.mProgressPaint.setColor(ContextCompat.getColor(this.getContext(), R.color.white));
        this.mProgressPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.mProgressPaint.setAntiAlias(true);
        this.mProgressPaint.setStrokeWidth(2);
    }

    public void setProgress(float progress) {
        mProgress = progress;
        this.invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {

        final float radius = mSideSize / 2;;
        final float centerX = radius;
        final float centerY = radius;
        canvas.drawCircle(centerX, centerY, radius, mBgPaint);


        /*
        * @param left   The X coordinate of the left side of the rectangle
        * @param top    The Y coordinate of the top of the rectangle
        * @param right  The X coordinate of the right side of the rectangle
        * @param bottom The Y coordinate of the bottom of the rectangle
        */
        RectF arcBounds = new RectF(
                centerX - radius, // The X coordinate of the left side of the rectangle
                centerY - radius, // The Y coordinate of the top of the rectangle
                centerX + radius, // The X coordinate of the right side of the rectangle
                centerY + radius);// The Y coordinate of the bottom of the rectangle
        // draw any progress over the top
        // why is this BigDecimal crap even needed? java why?
        // Default value is 10/100
        final BigDecimal percentage = BigDecimal.valueOf(mProgress);
        Log.i("CIRCULAR", "Percentage : " + percentage.toString());
        final BigDecimal sweepAngle = percentage.multiply(BigDecimal.valueOf(360));
        Log.i("CIRCULAR", "Sweep angle : " + percentage.toString());

        // bounds are same as the bg btn_circle, so diameter width and height moved in by margin
        canvas.drawArc(arcBounds, ADJUST_FOR_12_OCLOCK, sweepAngle.floatValue(), true,
                this.mProgressPaint);
    }


    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.mSideSize = parentWidth > parentHeight ? parentHeight : parentWidth;
        // size will always be diameter + margin on add sides
        final int size = this.mSideSize;
        this.setMeasuredDimension(size, size);
    }

    public void setThumbnail(Bitmap thumbnail, float uploadProgress) {

    }


    public void setError() {

    }
}

