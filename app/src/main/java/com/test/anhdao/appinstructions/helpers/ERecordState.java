package com.test.anhdao.appinstructions.helpers;

/**
 * Created by anhdao on 9/25/16.
 */
public enum ERecordState {
    OnStandByReady, Recording, Paused, Recorded
}
