package com.test.anhdao.appinstructions.helpers;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by anhdao on 9/25/16.
 */
public class BitmapHelper {


    public static Bitmap getBitmapFromFile(String path) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory
                .decodeFile(path, bmOptions);
        return bm;
    }

    public static void saveImageToFile(Bitmap bitmap, File file) {
        if (file.exists()) {
            file.delete();
        }
        FileOutputStream out = null;
        try {
            file.createNewFile();
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Bitmap resizeToThumbnail(Bitmap bitmap) {
        Bitmap resizedBitmap = null;
        int scaleSize = 568;
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        int newWidth = -1;
        int newHeight = -1;
        float multFactor = -1.0F;
        if(originalHeight > originalWidth) {
            newHeight = scaleSize ;
            multFactor = (float) originalWidth/(float) originalHeight;
            newWidth = (int) (newHeight*multFactor);
        } else if(originalWidth > originalHeight) {
            newWidth = scaleSize ;
            multFactor = (float) originalHeight/ (float)originalWidth;
            newHeight = (int) (newWidth*multFactor);
        } else if(originalHeight == originalWidth) {
            newHeight = scaleSize ;
            newWidth = scaleSize ;
        }
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }

    public static Bitmap getVideoFrameFromVideo(File pFile, long time)
            throws Throwable {
        if (!pFile.exists()) {
            return null;
        }
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(pFile.getAbsolutePath());
            bitmap = mediaMetadataRetriever.getFrameAtTime(time);
        } catch (Exception m_e) {
            m_e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String p_videoPath)"
                            + m_e.getMessage());
        } finally {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    private static final int INITIAL_SAMPLE_SIZE = 2;
    private static final int SAMPLE_SIZE_INCREMENT = 4;
    private static final String ASSET_PATH = "file:///android_asset/";

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = INITIAL_SAMPLE_SIZE;

        Log.i("Sample Size", String.format("Required WxH are: %d x %d. Size is : %d x %d",
                reqWidth, reqHeight, width, height));
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / SAMPLE_SIZE_INCREMENT;
            final int halfWidth = width / SAMPLE_SIZE_INCREMENT;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= SAMPLE_SIZE_INCREMENT;
            }
        }

        return inSampleSize;
    }

    public static Bitmap getImage(Context c, String assetFile, int reqWidth, int reqHeight) {
        try {
            InputStream bitmap = c.getAssets().open(assetFile);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(bitmap, null, options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            Log.i("Sample Size", String.format("Required WxH are: %d x %d. InSampleSize is : %d",
                    reqWidth, reqHeight, options.inSampleSize));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(bitmap, null, options);
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (NullPointerException exc) {
            exc.printStackTrace();
        }
        return null;
    }

}
