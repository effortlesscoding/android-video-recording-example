package com.test.anhdao.appinstructions.helpers;

import java.io.File;

/**
 * Created by anhdao on 9/25/16.
 */
public class RecordingOptions {
    private File mVideoFile;
    private File mThumbnailFile;
    private int mMaxRecordSeconds;

    public RecordingOptions(File videoFile, File thumbnailFile, int maxSeconds) {
        this.mMaxRecordSeconds = maxSeconds;
        this.mVideoFile = videoFile;
        this.mThumbnailFile = thumbnailFile;
    }

    public File getVideoFile() {
        return mVideoFile;
    }

    public File getThumbnailFile() {
        return mThumbnailFile;
    }

    public int getMaxSeconds() {
        return mMaxRecordSeconds;
    }
}
