package com.test.anhdao.appinstructions.helpers;

import android.content.Context;

import java.io.File;

/**
 * Created by anhdao on 9/25/16.
 */
public class FilesManager {

    public static File getVideoFile(Context c) {
        return new File(c.getFilesDir(), "video/temporary.mp4");
    }

    public static File getThumbnailFile(Context c) {
        return new File(c.getFilesDir(), "video/temporary.jpg");
    }
}
