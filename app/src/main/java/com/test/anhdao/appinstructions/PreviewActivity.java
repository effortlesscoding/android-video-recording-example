package com.test.anhdao.appinstructions;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.VideoView;

import com.test.anhdao.appinstructions.helpers.FilesManager;

import java.io.File;

/**
 * Created by anhdao on 9/25/16.
 */
public class PreviewActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();
    private VideoView mVideoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_preview);
        mVideoView = (VideoView)findViewById(R.id.vpVideoPreview);

        File video = FilesManager.getVideoFile(this);
        File thumbnail = FilesManager.getThumbnailFile(this);
        if (video.exists() && thumbnail.exists()) {
            Log.i(TAG, video.toString());
            Log.i(TAG, "Local video file's full path: " + video.getAbsolutePath());
            Log.i(TAG, "Local video file's existence: " + video.exists());
            Log.i(TAG, "Local video file's length: " + video.length());
            Log.i(TAG, "Local thumbnail file's full path: " + thumbnail.getAbsolutePath());
            Log.i(TAG, "Local thumbnail file's existence: " + thumbnail.exists());
            Log.i(TAG, "Local thumbnail file's length: " + thumbnail.length());
            mVideoView.setVideoPath(video.toString());
            mVideoView.start();
        }

    }
}
