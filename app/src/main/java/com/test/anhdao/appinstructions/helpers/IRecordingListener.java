package com.test.anhdao.appinstructions.helpers;

/**
 * Created by anhdao on 9/25/16.
 */
public interface IRecordingListener {
    void recordingStarted();
    void recordingResumed();
}
