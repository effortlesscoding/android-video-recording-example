package com.test.anhdao.appinstructions.helpers;

import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.SurfaceView;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anhdao on 9/25/16.
 */
public class CamVideoRecorder {
    private RecordingOptions mRecordingOptions;
    private String TAG = "PROFILE_VIDEO_RECORDER";
    private Camera mCamera;
    private ERecordState mCurrentRecordState;
    private WeakReference<SurfaceView> mPreviewSurface;
    private WeakReference<IRecordingListener> mRecordingDelegate;
    private MediaRecorder mRecorder;
    private final int DESIRED_WIDTH = 640, DESIRED_HEIGHT = 480;
    private int mCameraId;
    private List<Camera.Size> mSupportedPreviewSizes = new ArrayList<>();
    private List<Camera.Size> mSupportedVideoSizes = new ArrayList<>();
    private CamcorderProfile mCamProfile;
    private File f;
    /*
    I think the main interface is "callbackListener / delegate"
    Initialise
    Re-initialise
    Start recording
    Pause recording
    Stop
    Dispose
     */
    public CamVideoRecorder(IRecordingListener recordingDelegate, SurfaceView surface,
                            RecordingOptions options) {
        // Maybe just hold it as weak reference, though
        mRecordingOptions = options;
        mPreviewSurface = new WeakReference<SurfaceView>(surface);
        mRecordingDelegate = new WeakReference<IRecordingListener>(recordingDelegate);
    }

    public boolean startPreview() {
        try {
            mCamera = initialiseAndChooseCamera();
            mCurrentRecordState = ERecordState.OnStandByReady;
            if (mCamera != null) {
                loadAvailableDimensions();
                startCameraPreview();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return true;
        }
    }

    private void loadAvailableDimensions() {
        mCamProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        Camera.Parameters p = mCamera.getParameters();
        mSupportedVideoSizes = p.getSupportedVideoSizes();
        sortSmallestFirst();
        p.setPreviewSize(mCamProfile.videoFrameWidth, mCamProfile.videoFrameHeight);
        p.setRecordingHint(false);
        mCamera.setParameters(p);
    }

    private void startRecording(boolean unpause) {
        try {
            SurfaceView surfaceView = mPreviewSurface.get();
            IRecordingListener recordingDelegate = mRecordingDelegate.get();
            if (surfaceView != null && recordingDelegate != null) {
                initRecorder();
                f = mRecordingOptions.getVideoFile();
                if (!f.exists()) {
                    if (!f.getParentFile().exists()) {
                        f.getParentFile().mkdirs();
                    }
                    try {
                        f.createNewFile();
                        Log.i(TAG, f.getAbsolutePath() + "");
                        Log.i(TAG, f.exists() + "");
                        Log.i(TAG, f.length() + "");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Log.i(TAG, "Setting output file to : " + f.getPath());
                mRecorder.setOutputFile(f.getAbsolutePath());
                mRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
                mRecorder.prepare();
                mRecorder.start();
                if (!unpause) {
                    recordingDelegate.recordingStarted();
                } else {
                    recordingDelegate.recordingResumed();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void startCameraPreview() {
        try {
            SurfaceView surfaceView = mPreviewSurface.get();
            if (surfaceView != null) {
                mCamera.lock();
                mCamera.setDisplayOrientation(90);
                mCamera.setPreviewDisplay(surfaceView.getHolder());
                mCamera.startPreview();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ERecordState getRecordState() {
        return mCurrentRecordState;
    }

    private void initRecorder() {
        Log.i(TAG, "Initialized a recorder");
        mCamera.stopPreview();
        mCamera.unlock();
        mRecorder = new MediaRecorder();
        mRecorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
            @Override
            public void onError(MediaRecorder mr, int what, int extra) {
                int i = 0;
                Log.e(TAG, "Media recorder error: " + what + " - " + extra);
            }
        });
        mRecorder.setCamera(mCamera);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        try {
            mRecorder.setOrientationHint(270);
            mRecorder.setProfile(mCamProfile);
            mRecorder.setVideoEncodingBitRate(3000000);
            Camera.Size smallestSize = getSmallestAcceptableSize();
            mRecorder.setVideoSize(smallestSize.width, smallestSize.height);
            mRecorder.setMaxDuration(mRecordingOptions.getMaxSeconds() * 1000);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private Camera.Size getSmallestAcceptableSize() {
        for (int i = 0; i < mSupportedVideoSizes.size(); i++) {
            Camera.Size iSize = mSupportedVideoSizes.get(i);
            if (iSize.width < 480) {
                continue;
            } else {
                return iSize;
            }
        }
        return mSupportedVideoSizes.get(0);
    }
    private void sortSmallestFirst() {
        for (int i = 0; i < mSupportedVideoSizes.size(); i++) {
            for (int k = i; k < mSupportedVideoSizes.size(); k++) {
                Camera.Size kSize = mSupportedVideoSizes.get(k);
                Camera.Size iSize = mSupportedVideoSizes.get(i);
                if (kSize.height < iSize.height) {
                    mSupportedVideoSizes.set(i, kSize);
                    mSupportedVideoSizes.set(k, iSize);
                }
            }
        }
    }

    public void switchToRecordState(ERecordState newState) {
        switch (newState) {
            case Paused:
                if (mCurrentRecordState == ERecordState.Recording) {
                    stopRecording();
                }
                break;
            case OnStandByReady:
                if (mCurrentRecordState == ERecordState.Recording) {
                    stopRecording();
                }
                break;
            case Recorded:
                stopRecording();
                break;
            case Recording:
                if (mCurrentRecordState == ERecordState.OnStandByReady) {
                    Log.i(TAG, "Started recording.");
                    mCamera.stopPreview();
                    startRecording(false);
                } else if (mCurrentRecordState == ERecordState.Paused) {
                    Log.i(TAG, "Unpausing");
                    startRecording(true);
                }
                break;
        }
        mCurrentRecordState = newState;
    }

    public void stopRecording() {
        if (mRecorder != null) {
            try {
                mRecorder.stop();
                File thumbnailFile = mRecordingOptions.getThumbnailFile();
                if (thumbnailFile != null) {
                    File videoFile = mRecordingOptions.getVideoFile();
                    Bitmap thumbnailImage = BitmapHelper.resizeToThumbnail(
                            BitmapHelper.getVideoFrameFromVideo(
                                    videoFile,
                                    1000)
                    );
                    BitmapHelper.saveImageToFile(thumbnailImage, thumbnailFile);

                    Log.i(TAG, thumbnailFile.getAbsolutePath() + "");
                    Log.i(TAG, thumbnailFile.exists() + "");
                    Log.i(TAG, thumbnailFile.length() + "");
                    Log.i(TAG, videoFile.getAbsolutePath() + "");
                    Log.i(TAG, videoFile.exists() + "");
                    Log.i(TAG, videoFile.length() + "");
                }

            } catch (Throwable e){
                f.delete();
                e.printStackTrace();
            } finally {
                mRecorder.reset();
                mRecorder.release();
                mRecorder = null;
            }
        }
    }


    private Camera.Size getOptimalVideoSize(List<Camera.Size> availableSizes, int desiredHeight,
                                            int desiredWidth) {
        Camera.Size optimalSize = null;

        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) desiredHeight / desiredWidth;

        // Try to find a size match which suits the whole screen minus the menu on the left.
        for (Camera.Size size : availableSizes) {
            if (size.height > desiredHeight || size.width > desiredWidth) continue;
            optimalSize = size;
        }

        // If we cannot find the one that matches the aspect ratio, ignore the requirement.
        if (optimalSize == null) {
            // TODO : Backup in case we don't get a size.
        }

        return optimalSize;
    }

    private Camera initialiseAndChooseCamera() throws RuntimeException {
        Camera cam = null;
        try {
            int cameraCount = 0;
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            cameraCount = Camera.getNumberOfCameras();
            for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
                Camera.getCameraInfo(camIdx, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    cam = Camera.open(camIdx);
                    mCameraId = camIdx;
                }
            }

            // Throw an Exception
            if (cam == null) { throw new RuntimeException("Camera could not be found."); }

        } catch (Exception e) {
            Log.e(TAG, "Camera Error : ", e);
            e.printStackTrace();
        }
        return cam;
    }
}
